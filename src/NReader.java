import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class NReader {
	
	/**
	 * Maakt een nieuw NReader object om de buurgemeentes in te lezen uit n.xlsx.
	 * @param 	vlaanderen
	 * 			| Het Vlaanderen waarvoor we de gegevens van buurgemeentes inlezen.
	 * @post	Het vlaanderen object van deze reader is het opgegeven vlaanderen object.
	 */
	public NReader(Vlaanderen vlaanderen) {
		this.vlaanderen = vlaanderen;
	}
	
	Vlaanderen vlaanderen;

	/**
	 * Leest buurgemeentes in uit het n.xlsx bestand (pad op te geven in code zelf).
	 * @pre		Deze methode gaat ervanuit dat alle gemeentes al ingelezen zijn in het Vlaanderen object.
	 */
	public void readNeighbours() {
		
		try {
			// Bestandslocatie Windows.
			FileInputStream file = new FileInputStream(new File("C:\\data\\tvoo\\n.xlsx"));
			
			// Bestandslocatie Mac.
			//FileInputStream file = new FileInputStream(new File("/Users/nickscheynen/Documents/tvoo/n.xlsx"));
			
			// Bestandslocatie Linux.
			//FileInputStream file = new FileInputStream(new File("/home/nick/Documents/tvoo/n.xlsx"));
			             
			//Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            
            // Counter om bij te houden op welke rij we zitten.
            int ri = 0;
            
            // Lijst met, in volgorde, de stad objecten uit de kolom hoofdingen.
            ArrayList<City> cityArray = new ArrayList<City>();
            
            // We lezen het bestand rij per rij in van boven naar onder.
            while (rowIterator.hasNext()) 
            {	
            	Row row = rowIterator.next();
            	
            	// Counter om bij te houden in welke kolom we zitten.
            	int ci = 0;
            	
            	// We lopen door de kolommen.
            	Iterator<Cell> cellIterator = row.cellIterator();
            	
            	// De eerste rij is de rij met kolomhoofdingen. Hier construeren we de lijst met steden in de juiste volgorde.
            	if(ri == 0) {
            		
            		cellIterator = row.cellIterator();
            		
            		// We lopen door alle cellen van deze rij. 
            		while(cellIterator.hasNext()) {
            			Cell cell = cellIterator.next();
            			
            			// In de eerste kolom staan de rijnamen. Deze heeft geen kolomnaam. 
            			// In alle andere kolommen staat de naam van een stad.
            			if(ci != 0) {
            				// We lezen de naam van de stad, 
            				// we zoeken het corresponderende stadobject in Vlaanderen en
            				// we voegen dit toe aan het einde van onze stadlijst.
            				cityArray.add( vlaanderen.findCity(cell.getStringCellValue()) );
            			}
            			
            			// We verhogen de kolomteller.
            			ci = ci + 1;
            		}
            		
            	} 
            	// Alle andere rijen representeren de steden waarvoor we de buren in gaan lezen.
            	else {
            	
            		cellIterator = row.cellIterator();
            		
            		// In de eerste cel staat de naam van de stad waarvoor we de buren inlezen.
            		// We lezen de naam, zoeken het corresponderend stadobject en bewaren dit in de city variabele.
            		City city = vlaanderen.findCity( row.getCell(0).getStringCellValue() );
            		
            		// We lezen elke cel van deze rij.
            		while(cellIterator.hasNext()) {
            			Cell cell = cellIterator.next();
            			// De eerste cel bevat geen gegevens omtrent buurgemeentes.
            			if(ci != 0) {
            				// Als we een 1 inlezen, 
            				// dan voegen we de stad van de kolomnaam toe als buurgemeente van de stad van deze rij. 
            				if( cell.getNumericCellValue() == 1) {
            					// We verminderen de komteller met 1 omdat we de eerste kolom niet gelezen hebben
            					// hiervoor is dus ook geen item in de stadslijst.
            					city.addNeighbour( cityArray.get(ci-1) );
            				}
            			}
            			
            			// Verhogen van de kolomteller.
            			ci = ci + 1;
            		}
            		
            	}
                
            	// Verhogen van de rijteller.
                ri = ri + 1;
            }
            file.close();
			
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

	}
	
}
