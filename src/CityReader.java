import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class CityReader {
	
	/**
	 * Maakt een nieuw CityReader object om de steden in te lezen uit cities.xlsx.
	 * @param 	Vlaanderen
	 * 			| Het vlaanderen object waarvoor we de steden zullen inlezen.
	 * @post	Het Vlaanderen object van deze reader is het gegeven Vlaanderen object.
	 */
	public CityReader(Vlaanderen vlaanderen) {
		this.vlaanderen = vlaanderen;
	}
	
	Vlaanderen vlaanderen;
	
	/**
	 * Leest de steden in uit cities.xlsx (pad op te geven in de methode zelf)
	 */
	public void readCities() {
		
		try {
			// Bestandslocatie Windows
			FileInputStream file = new FileInputStream(new File("C:/data/tvoo/cities.xlsx"));
			
			// Bestandslocatie Mac
			//FileInputStream file = new FileInputStream(new File("/Users/nickscheynen/Documents/tvoo/cities.xlsx"));
			
			// Bestandslocatie Linux
			//FileInputStream file = new FileInputStream(new File("/home/nick/Documents/tvoo/cities.xlsx"));
			             
			//Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) 
            {
                Row row = rowIterator.next();
                
                // Elke rij is een nieuwe stad met in de eerste kolom de naam en de tweede kolom het aantal inwoners.
                // Voor elke rij maken we een nieuw stadobject met deze twee parameters.
                City city = new City( row.getCell(0).getStringCellValue(), (int)row.getCell(1).getNumericCellValue());
                
                // Elke stad wordt toegevoegd aan het Vlaanderen object van deze reader.
                this.vlaanderen.addCity(city);
            }
            file.close();
			
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

}
