import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class City {
	
	/**
	 * Maakt een nieuw stad object.
	 * @param 	name
	 * 			| Naam van de stad.
	 * @param 	inhabitants
	 * 			| Aantal inwoners van de stad.
	 * @post	De naam van deze stad is de opgegeven naam.
	 * @post	Het aantal inwoners van deze stad is het opgegeven aantal inwoners.
	 */
	public City(String name, int inhabitants) {
		this.setName(name);
		this.setInhabitants(inhabitants);
	}
	
	/**
	 * Zet de naam van deze stad naar de opgegeven naam.
	 * @param 	name
	 * 			| De naam van deze stad.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Geeft de naam van deze stad.
	 */
	public String getName() {
		return this.name;
	}
	
	String name;
	
	/**
	 * Zet het aantal inwoners van deze stad.
	 * @param	inhabitants
	 * 			| Aantal inwoners van deze stad.
	 */
	public void setInhabitants(int inhabitants) {
		this.inhabitants = inhabitants;
	}
	
	/**
	 * Geeft het aantal inwoners van deze stad.
	 */
	public int getInhabitants() {
		return this.inhabitants;
	}
	
	int inhabitants;
	
	public void addNeighbour(City city) {
		neighbours.add(city);
	}
	
	public ArrayList<City> getNeighbours() {
		return this.neighbours;
	}
	
	ArrayList<City> neighbours = new ArrayList<City>();
	
	/**
	 * Zet het key-value paar (andere) stad, economische coëfficiënt in de HashMap met stad-coëfficiënten van deze stad. 
	 * @param 	city
	 * 			| De stad waarvoor we de economische coëfficiënt opslaan.
	 * @param 	coef
	 * 			| De economische coëfficiënt van de andere stad met deze stad.
	 */
	public void addECoef(City city, double coef) {
		this.eccoefs.put(city, coef);
	}
	
	/**
	 * Geeft de economische coëfficiënt van de opgegeven stad met deze stad.
	 * @param 	city
	 * 			| Het stadobject waarvoor we de economische coëfficiënt met deze stad willen weten.
	 */
	public double getECoef(City city) {
		return eccoefs.get(city);
	}
	
	HashMap<City, Double> eccoefs = new HashMap<City, Double>();
	
	/**
	 * Stelt de cluster in waartoe deze stad behoort en zet de hasCluster flag op true.
	 * @param 	cluster
	 * 			| De cluster waartoe deze stad behoord.
	 */
	public void addToCluster(Cluster cluster) {
		this.cluster = cluster;
		this.hasCluster = true;
	}
	
	/**
	 * Verwijdert de cluster waartoe deze stad behoort en zet de hasCluster flag op false.
	 */
	public void removeCluster() {
		this.cluster = null;
		this.hasCluster = false;
	}
	
	/** 
	 * Geeft het Cluster object waartoe deze stad behoort.
	 */
	public Cluster getCluster() {
		return this.cluster;
	}
	
	/**
	 * Geeft of deze stad in een cluster zit of niet.
	 */
	public boolean hasCluster() {
		return this.hasCluster;
	}
	
	public Cluster cluster;
	public boolean hasCluster = false;
	
	/**
	 * Geeft de afstand van deze stad tot de opgegeven stad. 
	 * @param 	city
	 * 			| Stad tot de welke we de afstand willen weten.
	 * @param 	dist
	 * 			| Voorlopige aantal stappen die we al doorlopen hebben.
	 * @param 	scores
	 * 			| HashMap met steden en voorlopig minimum van stappen om deze te kunnen bereiken.
	 */
	public int getDistance(City city, int dist, HashMap<City, Integer> scores) {
		ArrayList<City> nbs = this.getNeighbours();
		for (Iterator<City> iterator = nbs.iterator(); iterator.hasNext();) {
			City nb = (City) iterator.next();
			if(scores.get(nb) == null) { scores.put(nb, Integer.MAX_VALUE); }
			if(scores.get(nb) > dist ) {
				scores.put(nb, dist);
				return getDistance(nb, dist+1, scores);
			}
		}
		return scores.get(city);
	}
	
}
