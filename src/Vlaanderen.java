import java.util.ArrayList;
import java.util.Iterator;

public class Vlaanderen {

	/**
	 * Maakt een nieuw Vlaanderen object aan.
	 * @post	Steden zijn ingelezen.
	 * @post	Gegevens omtrent buurgemeentes zijn ingelezen.
	 * @post	Economische gegevens zijn ingelezen.
	 */
	public Vlaanderen() {
		CityReader cityReader = new CityReader(this);
		cityReader.readCities();
		
		NReader nReader = new NReader(this);
		nReader.readNeighbours();
		
		CReader creader = new CReader(this);
		creader.readCoefs();
	}
	
	/**
	 * We maken voor elke stad in Vlaanderen een eigen cluster en voegen deze toe aan onze verzameling clusters (ClusterSet).
	 */
	public void setClustersOne() {
		// We lopen door alle steden van dit Vlaanderen.
		for (Iterator<City> iterator = this.getCities().iterator(); iterator.hasNext();) {
			City city = (City) iterator.next();
			
			// We maken een nieuwe cluster (in dit Vlaanderen)
			Cluster cluster = new Cluster(this);
			
			// We voegen de huidige stad toe aan de cluster.
			cluster.addCity(city);
			
			// We voegen de nieuwe cluster toe aan de clusterset van dit Vlaanderen.
			this.clusters.addCluster(cluster);
		}
	}
	
	/**
	 * We zetten de verzameling clusters van dit Vlaanderen op de opgegeven clusterset.
	 * @param 	newClusterSet
	 * 			| De clusterset voor dit vlaanderen.
	 */
	public void setClusterSet(ClusterSet newClusterSet) {
		this.clusters = newClusterSet;
	}
	
	/**
	 * Geeft de verzameling clusters van dit Vlaanderen.
	 */
	public ClusterSet getClusterSet() {
		return clusters;
	}
	
	ClusterSet clusters = new ClusterSet(this); 
	
	/**
	 * Voegt een stad toe aan de lijst met steden van dit Vlaanderen.
	 * @param 	city
	 * 			| Toe te voegen stad.
	 */
	public void addCity(City city) {
		cities.add(city);
	}
	
	ArrayList<City> cities = new ArrayList<City>();
	
	/**
	 * Als we de stad vinden geeft deze methode het corresponderende stad object.
	 * @param 	search
	 * 			| De naam van de stad naar de welke we zoeken. 
	 */
	public City findCity(String search) {
		City result = null;
		
		// We lopen doorheen alle steden in dit Vlaanderen.
		for (Iterator<City> iterator = cities.iterator(); iterator.hasNext();) {
			City city = (City) iterator.next();
			// Als de naam van de stad matcht met de zoekterm, dan wordt deze het resultaat en stoppen we de loop.
			if(city.getName().equals(search)) {
				result = city;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Geeft de lijst met steden van dit Vlaanderen.
	 */
	public ArrayList<City> getCities() {
		return cities;
	}
	
}
