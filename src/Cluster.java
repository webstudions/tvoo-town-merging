import java.util.ArrayList;
import java.util.Iterator;

import com.gs.collections.api.set.MutableSet;
import com.gs.collections.api.set.UnsortedSetIterable;
import com.gs.collections.impl.set.mutable.UnifiedSet;


public class Cluster implements Cloneable {
	
	/**
	 * Maakt een nieuwe cluster in het opgegeven vlaanderenobject.
	 * @param 	vlaanderen
	 * 			| Het Vlaanderen waarin deze cluster zich bevindt.
	 */
	public Cluster(Vlaanderen vlaanderen) {
		Cluster.vlaanderen = vlaanderen;
	}
	
	private static Vlaanderen vlaanderen;
	
	/**
	 * Voegt een stad toe aan deze cluster en voegt deze cluster toe aan het stadobject.
	 * @param 	city
	 * 			| De stad die we willen toevoegen aan deze cluster.
	 */
	public void addCity(City city) {
		this.getCities().add(city);
		city.addToCluster(this);
	}
	
	/**
	 * Verwijdert de gegeven stad uit deze cluster en verwijdert deze cluster uit de stad. 
	 * @param 	city
	 * 			| De te verwijderen stad.
	 */
	public void removeCity(City city) {
		cities.remove(city);
		city.removeCluster();
	}
	
	/**
	 * Geeft de lijst met steden die zich in deze cluster bevinden.
	 */
	public ArrayList<City> getCities() {
		return this.cities;
	}
	
	ArrayList<City> cities = new ArrayList<City>();
	
	/**
	 * Berekent en geeft de economische coëfficient van deze cluster met de opgegeven cluster.
	 * @param 	cluster
	 * 			| De opgegeven cluster waarvoor we coëfficient willen berekenen.
	 */
	public double getECoef(Cluster cluster) {
		double result = 0;
		
		// We lopen door alle steden van deze cluster.
		for (Iterator<City> iterator = cities.iterator(); iterator.hasNext();) {
			
			City city = (City) iterator.next();
			// We lopen door alle steden van de opgegeven cluster.
			for (Iterator<City> iterator2 = cluster.getCities().iterator(); iterator2.hasNext();) {
				City otherCity = (City) iterator2.next();
				// We verhogen het resultaat met de economische coëfficient 
				// tussen de huidige stad van deze cluster en de huidige stad van de andere cluster.  
				result = result + city.getECoef(otherCity);
			}
		}
		return result;
	}
	
	/**
	 * Berekent en geeft de economische coëfficient van deze cluster met zijn buurclusters.
	 */
	public double getECoefNbs() {
		double result = 0;
		
		// We lopen door alle buren van deze cluster.
		for (Iterator<Cluster> iterator = this.getNeighbourClusters().iterator(); iterator.hasNext();) {
			Cluster nb = (Cluster) iterator.next();
			
				// We lopen door alle steden van de huidige buurcluster.
				for (Iterator<City> iterator2 = nb.getCities().iterator(); iterator2.hasNext();) {
					City otherCity = (City) iterator2.next();
					
					// We lopen door alle steden van deze cluster.
					for (Iterator<City> iterator3 = this.getCities().iterator(); iterator3.hasNext();) {
						City city = (City) iterator3.next();
						// We verhogen het resultaat met de economische coëfficient 
						// tussen de huidige stad van deze cluster en de huidige stad van de andere cluster.  
						result = result + city.getECoef(otherCity);
					}
				}
		}
		return result;
	}
	
	/**
	 * Geeft het aantal inwoners van deze cluster.
	 */
	public int getPopulation() {
		int sum = 0;
		for (Iterator<City> iterator = this.getCities().iterator(); iterator.hasNext();) {
			City city = (City) iterator.next();
			sum = sum + city.getInhabitants();
		}
		return sum;
	}
	
	/**
	 * Geeft alle clusters die een buurcluster zijn van de deze cluster.
	 * Een buurcluster is een cluster waarbij minstens één stad van beide clusters de buur is 
	 * van een stad van de andere cluster.
	 */
	public ArrayList<Cluster> getNeighbourClusters() {
		
		ArrayList<Cluster> result = new ArrayList<Cluster>();
		ArrayList<Cluster> clusters = vlaanderen.getClusterSet().getClusters();
		
		// We lopen door alle clusters in Vlaanderen.
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			// Als de cluster niet deze cluster is, dan checken we of deze een buurcluster is.
			if(cluster != this) {
				
				// We lopen door alle steden van de huidige cluster.
				for (Iterator<City> iterator2 = cluster.getCities().iterator(); iterator2.hasNext();) {
					City otherCity = iterator2.next();
					
					// We lopen door alle steden van deze cluster.
					for (Iterator<City> iterator3 = this.getCities().iterator(); iterator3.hasNext();) {
						City city = iterator3.next();
						
						// Als de buursteden van huidige stad van deze cluster, de stad van de andere cluster 
						// bevat en deze cluster nog niet toegevoegd was,
						// dan voegen we deze andere cluster toe als een buurcluster.
						if( (city.getNeighbours().contains(otherCity)) && (!result.contains(cluster))) result.add(cluster);
					}
				}
			}
			
		}
		
		return result;
	}

	/**
	 * Geeft alle mogelijke subverzamelingen van steden.
	 */
	public MutableSet<UnsortedSetIterable<City>> getSubCities() {
		MutableSet<UnsortedSetIterable<City>> result = null;
		MutableSet<City> set = new UnifiedSet<City>();
		set.addAllIterable(this.getCities());
		result = set.powerSet();
		return result;
	}
	
	/**
	 * Geeft alle mogelijke subclusters van steden in deze cluster.
	 */
	public ArrayList<Cluster> getSubClusters() {
		
		ArrayList<Cluster> result = new ArrayList<Cluster>();
		MutableSet<UnsortedSetIterable<City>> subCities = this.getSubCities();
		
		// Loop door alle subsets van steden binnen deze cluster.
		for (Iterator<UnsortedSetIterable<City>> iterator = subCities.iterator(); iterator.hasNext();) {
			UnsortedSetIterable<City> unsortedSetIterable = (UnsortedSetIterable<City>) iterator.next();
			Cluster newCluster = new Cluster(vlaanderen);
			
			// Voeg alle steden van de stad subset toe aan de nieuwe cluster.
			for (Iterator<City> iterator2 = unsortedSetIterable.iterator(); iterator2.hasNext();) {
				City city = (City) iterator2.next();
				newCluster.addCity(city);
			}
			
			// Voeg de subcluster toe aan het resultaat.
			result.add(newCluster);
		}
		
		return result;
	}
	
	/**
	 * Geeft als resultaat of deze cluster een valide cluster is.
	 * Een valide cluster bestaat uit minstens 1 stad en in een valide cluster kan je vanuit eender welke stad
	 * alle andere steden in die cluster via buurgemeentes bereiken.
	 * @param 	city
	 * 			| Stad waar de methode is (start).
	 * @param 	prevCity
	 * 			| Vorige stad waar de methode was.
	 * @param 	toFind
	 * 			| Lijst met steden die nog 'gevonden' moet worden.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean isValidCluster(City city, ArrayList<City> wasHere, ArrayList<City> toFind) {
		
		// Als er nog geen steden 'bezocht' werden, dan maken we een nieuwe lijst aan.
		if(wasHere == null) wasHere = new ArrayList<City>();
		
		// Als het aantal steden in de cluster 0 is, dan is deze cluster niet correct.
		if(this.getCities().size() == 0) return false;
		
		// Als de city parameter niet ingevuld is (bij start), dan nemen we de eerste stad in de lijst.
		if(city == null) city = this.getCities().get(0);
		
		// Als de lijst met te vinden steden leeg is, dan nemen we alle steden uit deze cluster
		// zonder de stad waar we starten.
		if(toFind == null) {
			toFind = (ArrayList<City>) this.getCities().clone();
			toFind.remove(city);
		}
		
		// Voeg de huidige stad toe aan de lijst met de 'bezochte' steden.
		wasHere.add(city);
		
		// Als alle steden op de lijst gevonden zijn, dan is de cluster correct.
		if(toFind.size() == 0) return true;
		
		// We nemen de buurgemeentes van de huidige stad en lopen daardoor.
		ArrayList<City> nbs = city.getNeighbours();
		for (Iterator<City> iterator = nbs.iterator(); iterator.hasNext();) {
			City nb = (City) iterator.next();
			
			// Als de buurgemeente op de lijst staat met te vinden gemeentes
			// en de buurgemeente is nog niet 'bezocht'
			// en de buurgemeente staat op de lijst met te vinden steden.
			// dan verwijderen we de buurgemeente van de te vinden steden en roepen we de methode opnieuw op.
			if( (this.getCities().contains(nb)) && (!wasHere.contains(nb)) && toFind.contains(nb)) {
				toFind.remove(nb);
				return isValidCluster(nb, wasHere, toFind);
			}
		}
		
		// We geraken enkel hier als er niets meer te doorzoeken is en de lijst met te vinden steden
		// nog niet leeg is.
		return false;
	}

}
