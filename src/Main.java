import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/*
 * Klasse met de verschillende heuristieken.
 */
public class Main {
	
	// De statische variabele Vlaanderen omvat alle andere objecten die de realiteit en oplossing representeren
	static Vlaanderen vlaanderen;

	public static void main(String[] args) {
		
		// We maken een nieuw Vlaanderen object.
		vlaanderen = new Vlaanderen();
		
		// Om de onderliggende realiteit (steden) te behouden, stellen we de oplossing voor als verzameling van clusters.
		// Deze clusters zijn dan een verzameling van steden.
		// Initieel is de clusterverzameling gelijk aan de huidige situatie: elke stad zit in een eigen cluster.
		// We maken een cluster voor elke stad.
		vlaanderen.setClustersOne();
		
		// We printen de huigide situatie van de clusterset van vlaanderen.
		printSituation("BEFORE:");
		
		//analyseHeuristiek();

		heuristiek(vlaanderen, 152, 12, 16, 80);
		
		// We printen de oplossing.
		printSolution();
		
		// We printen de scores van de oplossing.
		printSituation("AFTER:");
		
		// We saven de oplossing in excel.
		try {
			System.out.println("Writing file.");
			vlaanderen.getClusterSet().writeSolution();
			System.out.println("File saved.");
		} catch (FileNotFoundException e) {
			System.out.println("Could not save file.");
		}
		
		// Zoek naar een stad en de andere steden in de cluster van deze stad.
		searchCitySolution();
		
	}
	
	/**
	 * Print het aantal clusters, de score en het aantal te kleine clusters in de clusterset van Vlaanderen.
	 * @param 	comment
	 * 			| Extra string om de locatie in het algoritme aan te duiden.
	 */
	public static void printSituation(String comment) {
		System.out.println("---------------------------");
		System.out.println(comment);
		System.out.println("NbClusters:"+vlaanderen.getClusterSet().getNbClusters());
		System.out.println("Score: "+vlaanderen.getClusterSet().getScore());
		System.out.println("NbTooSmall:"+vlaanderen.getClusterSet().getNbTooSmall());
		System.out.println("---------------------------");
	}
	
	/**
	 * Print alle clusters in Vlaanderen samen met de naam van de eerste stad en het aantal steden in de cluster.
	 */
	public static void printSolution() {
		int nbCities = 0;
		for (Iterator<Cluster> iterator = vlaanderen.getClusterSet().getClusters().iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			System.out.println("Cluster "+cluster.getCities().get(0).getName()+": "+cluster.getCities().size());
			nbCities = nbCities + cluster.getCities().size();
		}
	}
	
	/**
	 * Open een input lezer, zoek naar de stad van de user input en print hiervan de cluster informatie.
	 * Sluiten door exit te typen.
	 */
	public static void searchCitySolution() {
		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		String input = null;
		while(input != "exit") {
			System.out.println("Typ de naam van een gemeente en kijk welke andere gemeentes er in de cluster zitten:");
			input = scanner.nextLine();
			City found = vlaanderen.findCity(input);
			if(found != null) {
				System.out.println("In dezelfde cluster als "+input);
				for (Iterator<City> iterator = found.getCluster().getCities().iterator(); iterator.hasNext();) {
					City c = iterator.next();
					System.out.println(" - "+c.getName());
				}
				System.out.println("Press any key to search again:");
			}
		}
		scanner.close();
	}
	
	/**
	 * De oplossingsmethode voor ons probleem. Deze methode bestaat uit 4 stappen waarvan de essentie
	 * geprogrammeerd is in een andere methode van deze klasse. Deze submethodes hebben een naam
	 * die begint met de letter 'h'. 
	 * Methodes met een exponenti�le complexiteit hebben ook 'Deep' in hun naam.
	 */
	public static void heuristiek(Vlaanderen vlaanderen, int target, int maxOne, int maxTwo, int countTwo) {
		
		boolean status1 = true;
		while( (vlaanderen.getClusterSet().getSmallClusters().size() > 0) && status1) {
			status1 = hMergeBestSmallNbs(maxOne);
		}
		
		boolean status2 = true;
		while( (vlaanderen.getClusterSet().getNbClusters() > countTwo) && status2) {
			status2 = hMergeBestNbs(maxTwo);
		}
		
		int nbDeepSeparate = 0;
		while(vlaanderen.getClusterSet().getNbClusters() < target) {
			nbDeepSeparate = nbDeepSeparate + 1;
			printSituation("nbDeepSeparate: "+nbDeepSeparate);
			hDeepSeparate();
		}
		
		int nbDeepSwap = 0;
		double score = Double.POSITIVE_INFINITY;
		while( score > hDeepSwap()) {
			score = newDeepSwapScore;
			nbDeepSwap = nbDeepSwap + 1;
			printSituation("nbDeepSwap: "+nbDeepSwap);
		}
		
	}
	
	static double newDeepSwapScore;
	
	/**
	 * Sub methode van onze heuristiek die alle te kleine clusters in Vlaanderen afloopt en
	 * deze (binnen opgelegde beperkingen) samenvoegt met zijn beste buurcluster.
	 * 
	 * De beste buurcluster van een kleine cluster is de buurcluster waarmee de kleine cluster de hoogste
	 * economische verwantschap heeft en waarbij de samenvoeging van beide clusters een kleiner aantal 
	 * steden heeft dan het opgeven aantal. 
	 * @param 	maxNb
	 * 			| Maximum aantal steden per cluster na uitvoeren van deze methode.
	 */
	public static boolean hMergeBestSmallNbs(int maxNb) {
		// We nemen alle clusters in Vlaanderen die te klein zijn.
		ArrayList<Cluster> clusters = vlaanderen.getClusterSet().getSmallClusters();
		
		// Voorlopig maximale score
		double maxPCF = 0.0;
		// Voorlopige twee clusters met hoogste economische verwantheid
		Cluster cluster1 = null;
		Cluster cluster2 = null;
		
		// We lopen door alle te kleine clusters
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			// We lopen door alle buurclusters van de huidige te kleine cluster.
			ArrayList<Cluster> nbs = cluster.getNeighbourClusters();
			for (Iterator<Cluster> iterator2 = nbs.iterator(); iterator2.hasNext();) {
				Cluster nb = (Cluster) iterator2.next();
				
				// Als de huidige buurcluster een betere oplossing oplevert dan de voorlopige,
				// dan wordt dit de nieuwe voorlopig beste.
				if( (cluster.getECoef(nb) > maxPCF) && (cluster.getCities().size() + nb.getCities().size() <= maxNb) ) {
					maxPCF = cluster.getECoef(nb);
					cluster1 = cluster;
					cluster2 = nb;
				}
			}
		}
		
		// Als we alle kleine clusters afgelopen hebben, dan voegen we de kleine cluster waarvoor we de buurcluster
		// vonden met een hoogste economische verwantschap samen met deze cluster.
		if(cluster1 != null && cluster2 != null) {
			vlaanderen.getClusterSet().mergeClusters(cluster1, cluster2);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Sub methode van onze heuristiek die alle clusters in Vlaanderen afloopt en
	 * de combinatie (met een buurcluster) met de hoogste verwantschap samenvoegt.
	 * 
	 * De beste combinatie moet ook minder steden hebben dan vooraf opgegeven. 
	 * @param 	maxNb
	 * 			| Maximum aantal steden per cluster na uitvoeren van deze methode.
	 */
	public static boolean hMergeBestNbs(int maxNb) {
		// We nemen alle clusters in Vlaanderen.
		ArrayList<Cluster> clusters = vlaanderen.getClusterSet().getClusters();
		
		// Voorlopig maximale score
		double maxPCF = 0.0;
		// Voorlopige twee clusters met hoogste economische verwantheid
		Cluster cluster1 = null;
		Cluster cluster2 = null;
		
		// We lopen door alle clusters
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			// We lopen door alle buurclusters van de huidige cluster.
			ArrayList<Cluster> nbs = cluster.getNeighbourClusters();
			for (Iterator<Cluster> iterator2 = nbs.iterator(); iterator2.hasNext();) {
				Cluster nb = (Cluster) iterator2.next();
				
				// Als de huidige buurcluster een betere oplossing oplevert dan de voorlopige,
				// dan wordt dit de nieuwe voorlopig beste.
				if(cluster.getECoef(nb) > maxPCF && (cluster.getCities().size()+nb.getCities().size() <= maxNb)) {
					maxPCF = cluster.getECoef(nb);
					cluster1 = cluster;
					cluster2 = nb;
				}
			}
		}
		
		// Als we alle clusters afgelopen hebben, dan voegen we de twee clusters met hoogste economische
		// verwantheid samen.
		if(cluster1 != null && cluster2 != null) {
			vlaanderen.getClusterSet().mergeClusters(cluster1, cluster2);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Scheurt van alle mogelijke subclusters van alle clusters in Vlaanderen, diegene af die de minste
	 * economische verwantheid heeft met zijn oorspronkelijke cluster.
	 */
	public static void hDeepSeparate() {
		
		ArrayList<Cluster> clusters = vlaanderen.getClusterSet().getClusters();
		
		// Voorlopig minimale toegevoegde score bij afsplitsen van minCluster van oldCluster
		double minScore = Double.POSITIVE_INFINITY;
		Cluster oldCluster = null;
		Cluster minCluster = null;
		
		// We lopen door alle clusters in Vlaanderen.
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			if(cluster.getCities().size() <= 16) {
				
				// We nemen alle mogelijke subclusters van deze cluster en lopen daardoor.
				ArrayList<Cluster> subClusters = cluster.getSubClusters();
				for (Iterator<Cluster> iterator2 = subClusters.iterator(); iterator2.hasNext();) {
					Cluster subCluster = (Cluster) iterator2.next();
					
					// Als de subCluster een cluster is die afgezonder kan worden, dan nemen we deze in overweging.
					if(subCluster.isValidCluster(null, null, null)) {
						
						/* Remove cities in subCluster from cluster to do calculations */
						for (Iterator<City> iterator3 = subCluster.getCities().iterator(); iterator3.hasNext();) {
							City subCity = (City) iterator3.next();
							cluster.removeCity(subCity);
						}
						
						// We checken verder of de afscheuring van de subcluster geen ongewenste situatie zou creëren.
						// Wat we niet willen is: de subcluster is een te kleine cluster, de originele cluster heeft geen
						// steden meer en de oude cluster is na afscheuring een te kleine cluster.
						// Als voorgaande voldaan zijn en als de afscheuring van deze subcluster tot een betere score leidt 
						// dan de voorlopige, dan houden we deze bij.
						if( (subCluster.getECoef(cluster) < minScore)
								&& subCluster.getPopulation() > 15000
								&& cluster.getCities().size() >= 1
								&& cluster.getPopulation() > 15000) {
							minScore = subCluster.getECoef(cluster);
							minCluster = subCluster;
							oldCluster = cluster;
						}
					
						/* Re-add cities to cluster to restore initial situation */
						for (Iterator<City> iterator3 = subCluster.getCities().iterator(); iterator3.hasNext();) {
							City subCity = (City) iterator3.next();
							cluster.addCity(subCity);
						}
						
					}
				}
			}
		}
		
		// We vonden de beste cluster om af te scheuren en passen deze afscheuring nu echt toe op de oplossing.
		// We verwijderen de steden in de optimale subcluster van de oude cluster
		for (Iterator<City> iterator3 = minCluster.getCities().iterator(); iterator3.hasNext();) {
			City subCity = (City) iterator3.next();
			oldCluster.removeCity(subCity);
		}
		
		// We voegen de subcluster toe als volwaardige cluster.
		vlaanderen.getClusterSet().addCluster(minCluster);
		
	}
	
	/**
	 * Gaat voor alle clusters waarbij het mogelijk (>1 stad) en haalbaar is (<16 steden) alle mogelijke subclusters af
	 * en bekijkt voor elke subcluster de mogelijke clusters bij dewelke deze gevoegd kan worden. Vanaf het moment dat 
	 * er een swap gevonden wordt, die een betere totaaloplossing oplevert, wordt deze uitgevoerd.
	 * De methode geeft de nieuwe score als resultaat.
	 */
	public static double hDeepSwap() {

		// Voorlopige score
		double minPCF = vlaanderen.getClusterSet().getScore();
		// Cluster vanwaaruit er gewisseld moet worden.  
		Cluster originCluster = null;
		// Cluster naar waar de swap moet gebeuren.
		Cluster targetCluster = null;
		// Subcluster die verwisseld zal worden.
		Cluster swapCluster = null;
		// Flag om aan te geven dat er een verbetering gevonden is.
		boolean better = false;
		double result = 0.0;
		
		// We lopen door alle clusters in Vlaanderen.
		ArrayList<Cluster> clusters = vlaanderen.getClusterSet().getClusters();
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			// Als er geen subclusters zijn of als het aantal subclusters computationeel te groot wordt,
			// dan gaan we niet verder.
			if(cluster.getCities().size() > 1 && cluster.getCities().size() <= 16) {
				
				// We lopen door alle subclusters.
				ArrayList<Cluster> subClusters = cluster.getSubClusters();
				for (Iterator<Cluster> iterator2 = subClusters.iterator(); iterator2.hasNext();) {
					Cluster subCluster = (Cluster) iterator2.next();
					
					// Als de subcluster een correcte cluster is en de subcluster niet alle steden van de originele cluster bevat
					// dan gaan we verder.
					if(subCluster.isValidCluster(null, null, null) && (subCluster.getCities().size() < cluster.getCities().size()) ) {
						
						// We verwijderen de steden van de subcluster uit de originele cluster om de nieuwe situatie te cre�ren
						for (Iterator<City> iterator3 = subCluster.getCities().iterator(); iterator3.hasNext();) {
							City subCity = (City) iterator3.next();
							cluster.removeCity(subCity);
						}
						
						// We lopen door alle buurclusters van de subcluster.
						ArrayList<Cluster> nbs = subCluster.getNeighbourClusters();
						for (Iterator<Cluster> iterator3 = nbs.iterator(); iterator3.hasNext();) {
							Cluster nb = (Cluster) iterator3.next();
							
							// We voegen de steden van de subcluster toe aan de huidige buurcluster.
							for (Iterator<City> iterator4 = subCluster.getCities().iterator(); iterator4.hasNext();) {
								City subCity = (City) iterator4.next();
								nb.addCity(subCity);
							}
							
							// Als de nieuwe situatie een betere score heeft als voorheen en 
							// als de originele cluster geen te kleine cluster wordt,
							// dan houden we deze bij.
							if( (vlaanderen.getClusterSet().getScore() < minPCF)
									&& (cluster.getPopulation() - subCluster.getPopulation()) > 15000
							) {
								minPCF = vlaanderen.getClusterSet().getScore();
								originCluster = cluster;
								targetCluster = nb;
								swapCluster = subCluster;
								better = true;
								result = minPCF;
							}
							
							// We verwijderen de steden van de subcluster van de huidige buurcluster.
							for (Iterator<City> iterator5 = subCluster.getCities().iterator(); iterator5.hasNext();) {
								City subCity = (City) iterator5.next();
								nb.removeCity(subCity);
							}
							
							// Als we een betere oplossing gevonden hebben, dan stoppen de we loop.
							if(better) break;
								
						}
					
						// We herstellen de nieuwe situatie volledig.
						for (Iterator<City> iterator3 = subCluster.getCities().iterator(); iterator3.hasNext();) {
							City subCity = (City) iterator3.next();
							cluster.addCity(subCity);
						}
						
						// Als we een betere oplossing gevonden hebben, dan stoppen de we loop.
						if(better) break;
						
					}
				}
				
			}
			// Als we een betere oplossing gevonden hebben, dan stoppen de we loop.
			if(better) break;
		}
		
		// We passen de nieuwe, betere oplossing toe.
		if(swapCluster != null) {
			for (Iterator<City> iterator3 = swapCluster.getCities().iterator(); iterator3.hasNext();) {
				City subCity = (City) iterator3.next();
				originCluster.removeCity(subCity);
				targetCluster.addCity(subCity);
			}
		} else {
			result = vlaanderen.getClusterSet().getScore();
		}
		
		newDeepSwapScore = result;
		return result;
		
	}
	
	public static void analyseHeuristiek() {
		// We lossen het probleem op via onze heuristiek:
		int maxOne = 0;
		int bestOne = 0;
		int maxTwo = 0;
		int bestTwo = 0;
		int countTwo = 0;
		int bestCount = 0;
		double bestScore = Double.POSITIVE_INFINITY;
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("heuristiek");
		int rown = 0;
		Row row = sheet.createRow(rown);
		Cell cell1 = row.createCell(0);
		cell1.setCellValue("maxOne");
		Cell cell2 = row.createCell(1);
		cell2.setCellValue("maxTwo");
		Cell cell3 = row.createCell(2);
		cell3.setCellValue("countTwo");
		Cell cell4 = row.createCell(3);
		cell4.setCellValue("score");
		rown++;
		
		for(maxOne = 12; maxOne <= 15; maxOne = maxOne+1) {
			for(maxTwo = 16; maxTwo <= 16; maxTwo = maxTwo+4) {
				for(countTwo = 70; countTwo <= 80; countTwo = countTwo + 2) {
					vlaanderen = new Vlaanderen();
					vlaanderen.setClustersOne();
					
					System.out.println("Executing for: maxOne:"+maxOne+"; maxTwo:"+maxTwo+"; countTwo:"+countTwo);
					heuristiek(vlaanderen, 152, maxOne, maxTwo, countTwo);
					
					Row row1 = sheet.createRow(rown);
					Cell cell11 = row1.createCell(0);
					cell11.setCellValue(maxOne);
					Cell cell21 = row1.createCell(1);
					cell21.setCellValue(maxTwo);
					Cell cell31 = row1.createCell(2);
					cell31.setCellValue(countTwo);
					Cell cell41 = row1.createCell(3);
					cell41.setCellValue(vlaanderen.getClusterSet().getScore());
					rown++;
					
					if(vlaanderen.getClusterSet().getScore() < bestScore) {
						bestOne = maxOne;
						bestTwo = maxTwo;
						bestCount = countTwo;
						bestScore = vlaanderen.getClusterSet().getScore();
					}
				}
			}
		}
		
		FileOutputStream stream = null;
		try {
			//stream = new FileOutputStream(new File("/Users/nickscheynen/Documents/tvoo/heuristieken.xls"));
			//stream = new FileOutputStream(new File("/home/nick/Documents/tvoo/heuristieken.xls"));
			stream = new FileOutputStream(new File("C:/data/tvoo/heuristieken.xls"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			workbook.write(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Found best solution ("+bestScore+") at maxOne:"+bestOne+"; maxTwo:"+bestTwo+"; countTwo:"+bestCount);
		
	}
	
	
}