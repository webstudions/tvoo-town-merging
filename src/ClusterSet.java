import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;


public class ClusterSet implements Cloneable {
	
	/**
	 * Maakt een nieuwe verzameling aan van clusters.
	 * @param 	vlaanderen
	 * 			| Het vlaanderen waarin deze verzameling van clusters zich bevindt.
	 * @post	Het Vlaanderen van deze clusterset is het opgegeven Vlaanderen object.
	 */
	public ClusterSet(Vlaanderen vlaanderen) {
		ClusterSet.vlaanderen = vlaanderen;
	}
	
	public static Vlaanderen vlaanderen;
	
	/**
	 * Voegt de opgeven cluster toe aan deze clusterset. 
	 * @param 	cluster
	 * 			| De toe te voegen cluster.
	 */
	public void addCluster(Cluster cluster) {
		this.clusters.add(cluster);
	}
	
	/**
	 * Geeft het aantal clusters dat in deze verzameling zit.
	 */
	public int getNbClusters() {
		return this.clusters.size();
	}
	
	/**
	 * Geeft de lijst met clusters in deze clusterset.
	 */
	public ArrayList<Cluster> getClusters() {
		return this.clusters;
	}
	
	ArrayList<Cluster> clusters = new ArrayList<Cluster>();
	
	/**
	 * Verwijdert de opgegeven cluster uit deze clusterset. 
	 * @param 	cluster
	 * 			| De te verwijderen cluster.
	 */
	public void removeCluster(Cluster cluster) {
		this.getClusters().remove(cluster);
	}
	
	/**
	 * Voegt de twee opgegeven clusters samen door eerste alle steden van beide clusters toe te voegen
	 * aan een nieuwe cluster, dan de twee oude clusters te verwijderen en de nieuwe cluster toe te voegen.
	 * @param 	cluster1
	 * 			| Eerste samen te voegen cluster.
	 * @param 	cluster2
	 * 			| Tweede samen te voegen cluster.
	 */
	public void mergeClusters(Cluster cluster1, Cluster cluster2) {
		// De nieuwe cluster die we straks zullen toevoegen aan deze clusterset.
		Cluster newCluster = new Cluster(vlaanderen);
		
		// We lopen door alle steden van de eerste cluster en voegen deze toe aan de nieuwe cluster.
		for (Iterator<City> iterator = cluster1.getCities().iterator(); iterator.hasNext();) {
			City city = iterator.next();
			newCluster.addCity(city);
		}
		
		// We lopen door alle steden van de tweede cluster en voegen deze toe aan de nieuwe cluster.
		for (Iterator<City> iterator = cluster2.getCities().iterator(); iterator.hasNext();) {
			City city = iterator.next();
			newCluster.addCity(city);
		}
		
		// We verwijderen beide opgegeven clusters uit de huidige clusterset;
		this.removeCluster(cluster1);
		this.removeCluster(cluster2);
		
		// We voegen de nieuwe cluster toe aan deze clusterset.
		this.addCluster(newCluster);
	}
	
	/**
	 * Geeft het bevolkingsaantal van de kleinste cluster.
	 */
	public double getSmallestPopulation() {
		return this.getSmallestCluster().getPopulation();
	}
	
	/**
	 * Geeft het clusterobject van deze clusterset met de kleinste populatie.
	 */
	public Cluster getSmallestCluster() {
		Cluster minCluster = null;
		double minPop = Double.POSITIVE_INFINITY;
		
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			if(cluster.getPopulation()<minPop) {
				minCluster = cluster;
				minPop = cluster.getPopulation();
			}
		}
		
		return minCluster;
	}
	
	/**
	 * Geeft de lijst met clusters uit deze clusterset die een populatie hebben onder 15000.
	 */
	public ArrayList<Cluster> getSmallClusters() {
		ArrayList<Cluster> result = new ArrayList<Cluster>();
		
		for (Iterator<Cluster> iterator = this.getClusters().iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			if(cluster.getPopulation() < 15000) {
				result.add(cluster);
			}
		}
		return result;
	}
	
	/**
	 * Berekent en geeft de score die de huidige verzameling clusters haalt. 
	 * De score wordt berekend door de som te nemen van de economische coëfficienten tussen alle clusters.
	 * De economische coëfficient tussen twee clusters is de som van coëfficienten tussen alle steden van beide clusters.
	 */
	public double getScore() {
		double result = 0;
		
		// We lopen door alle clusters in deze clusterset.
		for (Iterator<Cluster> iterator = this.getClusters().iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			
			//We lopen opnieuw door alle clusters in deze clusterset.
			for (Iterator<Cluster> iterator2 = this.getClusters().iterator(); iterator2.hasNext();) {
				Cluster otherCluster = (Cluster) iterator2.next();
				
				// De economische coëfficient van een cluster met zichzelf telt niet mee in de berekening.
				if(cluster != otherCluster) {
					result = result + cluster.getECoef(otherCluster);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Geeft aantal clusters dat te klein is (inwoners < 15000).
	 */
	public int getNbTooSmall() {
		return getSmallClusters().size();
	}
	
	/**
	 * Geeft een clone van dit ClusterSet object.
	 */
	public Object clone(){  
	    try{  
	        return super.clone();  
	    }catch(Exception e){ 
	        return null; 
	    }
	}
	
	/**
	 * Schrijft de oplossing naar excel.
	 * @throws FileNotFoundException
	 */
	public void writeSolution() throws FileNotFoundException {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(""+this.getScore());
		
		Cluster prevCluster = null;
		int clusterNb = 0;
		int rown = 0;
		for (Iterator<Cluster> iterator = clusters.iterator(); iterator.hasNext();) {
			Cluster cluster = (Cluster) iterator.next();
			ArrayList<City> cities = cluster.getCities();
			for (Iterator<City> iterator2 = cities.iterator(); iterator2.hasNext();) {
				Row row = sheet.createRow(rown);
				City city = (City) iterator2.next();
				
				Cell cell1 = row.createCell(0);
				if( (cluster != prevCluster) ) {
					cell1.setCellValue("Cluster "+(clusterNb+1));
					prevCluster = cluster;
				}
				Cell cell2 = row.createCell(1);
				cell2.setCellValue( city.getName() );
				
				rown = rown + 1;
			}
			
			clusterNb++;
		}
		
		FileOutputStream stream = null;
		try {
			//stream = new FileOutputStream(new File("/Users/nickscheynen/Documents/tvoo/result.xls"));
			//stream = new FileOutputStream(new File("/home/nick/Documents/tvoo/result.xls"));
			stream = new FileOutputStream(new File("C:/data/tvoo/result.xls"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			workbook.write(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
