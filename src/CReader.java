import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class CReader {
	
	/**
	 * Maakt een nieuw CReader object om de economische coëfficiënten in te lezen uit c.xlsx.
	 * @param 	vlaanderen
	 * 			| Het Vlaanderen waarvoor we de economische gegevens inlezen.
	 * @post	Het vlaanderen object van deze reader is het opgegeven vlaanderen object.
	 */
	public CReader(Vlaanderen vlaanderen) {
		this.vlaanderen = vlaanderen;
	}
	
	Vlaanderen vlaanderen;

	/**
	 * Leest economische gegevens in uit het c.xlsx bestand (pad op te geven in code zelf).
	 * @pre		Deze methode gaat ervanuit dat alle gemeentes al ingelezen zijn in het Vlaanderen object.
	 */
	public void readCoefs() {
		
		try {
			// Bestandslocatie Windows.
			FileInputStream file = new FileInputStream(new File("C:/data/tvoo/c.xlsx"));
			
			// Bestandslocatie Mac.
			//FileInputStream file = new FileInputStream(new File("/Users/nickscheynen/Documents/tvoo/c.xlsx"));
			
			// Bestandslocatie Linux.
			//FileInputStream file = new FileInputStream(new File("/home/nick/Documents/tvoo/c.xlsx"));
			             
			//Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            
            // Counter om bij te houden op welke rij we zitten.
            int ri = 0;
            
            // Lijst met, in volgorde, de stad objecten uit de kolom hoofdingen.
            ArrayList<City> cityArray = new ArrayList<City>();
            
            // We lezen het bestand rij per rij in van boven naar onder.
            while (rowIterator.hasNext()) 
            {	
            	Row row = rowIterator.next();
            	
            	// Counter om bij te houden in welke kolom we zitten.
            	int ci = 0;
            	
            	// We lopen door de kolommen.
            	Iterator<Cell> cellIterator = row.cellIterator();
            	
            	// De eerste rij is de rij met kolomhoofdingen. Hier construeren we de lijst met steden in de juiste volgorde.
            	if(ri == 0) {
            		
            		// We lopen door alle cellen van deze rij.
            		cellIterator = row.cellIterator();
            		while(cellIterator.hasNext()) {
            			
            			Cell cell = cellIterator.next();
            			
            			// In de eerste kolom staan de rijnamen. Deze heeft geen kolomnaam. 
            			// In alle andere kolommen staat de naam van een stad.
            			if(ci != 0) {
            				// We lezen de naam van de stad, 
            				// we zoeken het corresponderende stadobject in Vlaanderen en
            				// we voegen dit toe aan het einde van onze stadlijst.
            				cityArray.add( vlaanderen.findCity(cell.getStringCellValue()) );
            			}
            			
            			// We verhogen de kolomteller.
            			ci = ci + 1;
            		}
            		
            	}
            	// Alle andere rijen representeren de steden waarvoor we de economische gegevens in gaan lezen.
            	else {
            		
            		cellIterator = row.cellIterator();
            		
            		// In de eerste cel staat de naam van de stad waarvoor we de buren inlezen.
            		// We lezen de naam, zoeken het corresponderend stadobject en bewaren dit in de city variabele.
            		City city = vlaanderen.findCity( row.getCell(0).getStringCellValue() );
            		
            		// We lezen elke cel van deze rij.
            		while(cellIterator.hasNext()) {
            			Cell cell = cellIterator.next();
            			
            			// De eerste cel bevat geen economische gegevens.
            			if(ci != 0) {
            				// We nemen de stad van de kolom en lezen de economische coëfficiënt van deze stad
            				// met de stad van de rij. Met de addECoef methode voegen we deze key-value pair 
            				// toe aan de stad van de rij.
            				city.addECoef(cityArray.get(ci-1), cell.getNumericCellValue());
            			}
            			
            			// Verhogen van de kolomteller.
            			ci = ci + 1;
            		}
            		
            	}
                
            	// Verhogen van de rijteller.
                ri = ri + 1;
            }
            file.close();
			
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

	}
	
}
